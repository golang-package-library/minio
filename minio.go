package minio

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/url"
	"os"
	"time"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	Env "gitlab.com/golang-package-library/env"
)

type MinioDefinition interface {
	Client() *minio.Client
	BucketExist(minioClient *minio.Client, bucketName string) bool
	CopyObject(minioClient *minio.Client, bucketNameSource string, objectNameSource string, bucketNameDestination string, objectNameDestination string) bool
	FGetObject(minioClient *minio.Client, bucketName string, objectName string, filePath string) bool
	GetBucketPolicy(minioClient *minio.Client, bucketName string) string
	GetObject(minioClient *minio.Client, bucketName string, objectName string, filePath string) bool
	ListObjects(minioClient *minio.Client, buckerName string) map[string]interface{}
	MakeBucket(minioClient *minio.Client, bucketName string, location string)
	ListIncompleteUploads(minioClient *minio.Client, bucketName string) map[string]interface{}
	ListBuckets(minioClient *minio.Client) (bucketInfo []minio.BucketInfo, err error)
	RemoveBucket(minioClient *minio.Client, bucketName string) bool
	SetBucketPolicy(minioClient *minio.Client, bucketName string, policy string) bool
	UploadObject(minioClient *minio.Client, bucketName string, objectName string, filePatch string, contentType string) (bool, error)
	PutObject(minioClient *minio.Client, bucketName string, objectName string, filePath string) bool
	StatObject(minioClient *minio.Client, bucketName string, objectName string) (objectInfo minio.ObjectInfo)
	RemoveObject(minioClient *minio.Client, bucketName string, objectName string) bool
	RemoveObjects(minioClient *minio.Client, bucketName string, prefixName string) bool
	RemoveIncompleteUpload(minioClient *minio.Client, bucketName string, objectName string) bool
	SelectObjectContent()
	SetAppInfo(minioClient *minio.Client, appName string, appVersion string) bool
	SignUrl(minioClient *minio.Client, bucketName string, objectName string, filename string) *url.URL
}

type Minio struct {
	Endpoint        string
	AccessKeyID     string
	SecretAccessKey string
	UseSSL          bool
	Context         context.Context
	MinioClient     *minio.Client
	Err             error
}

// (minioClient *minio.Client, err error)
func NewMinio(env Env.Env) Minio {
	newMinio := Minio{
		Endpoint:        env.Endpoint,
		AccessKeyID:     env.AcessKeyID,
		SecretAccessKey: env.SecretAccessKey,
	}

	if env.UseSsl == "yes" {
		newMinio.UseSSL = true
	} else {
		newMinio.UseSSL = false
	}

	minioClient, err := minio.New(newMinio.Endpoint, &minio.Options{
		Creds: credentials.NewStaticV4(newMinio.AccessKeyID, newMinio.SecretAccessKey, ""),
	})

	if err != nil {
		log.Fatalln(err)
	}

	return Minio{
		MinioClient: minioClient,
	}
}

func (m Minio) Client() *minio.Client {
	return m.MinioClient
}

// CopyObject implements MinioDefinition
func (m Minio) CopyObject(minioClient *minio.Client, bucketNameSource string, objectNameSource string, bucketNameDestination string, objectNameDestination string) bool {
	// Use-case 1: Simple copy object with no conditions.
	// Source object
	srcOpts := minio.CopySrcOptions{
		Bucket: bucketNameSource,
		Object: objectNameSource,
	}

	// Destination object
	dstOpts := minio.CopyDestOptions{
		Bucket: bucketNameDestination,
		Object: objectNameDestination,
	}

	// Copy object call
	_, err := minioClient.CopyObject(context.Background(), dstOpts, srcOpts)
	if err != nil {
		// fmt.Println(err)
		return false
	}

	// fmt.Println("Successfully copied object:", uploadInfo)
	return true
}

// FGetObject implements MinioDefinition
func (m Minio) FGetObject(minioClient *minio.Client, bucketName string, objectName string, filePath string) bool {
	err := minioClient.FGetObject(context.Background(), bucketName, objectName, filePath, minio.GetObjectOptions{})
	if err != nil {
		// fmt.Println(err)
		return false
	}
	return true
}

// GetBucketPolicy implements MinioDefinition
func (m Minio) GetBucketPolicy(minioClient *minio.Client, bucketName string) string {
	policy, err := minioClient.GetBucketPolicy(context.Background(), bucketName)
	if err != nil {
		log.Fatalln(err)
		return ""
	}
	return policy
}

// GetObject implements MinioDefinition
func (m Minio) GetObject(minioClient *minio.Client, bucketName string, objectName string, filePath string) bool {
	object, err := minioClient.GetObject(context.Background(), bucketName, objectName, minio.GetObjectOptions{})
	if err != nil {
		fmt.Println(err)
		return false
	}

	localFile, err := os.Create("/tmp/local-file.jpg")
	if err != nil {
		// fmt.Println(err)
		return false
	}

	if _, err = io.Copy(localFile, object); err != nil {
		// fmt.Println(err)
		return false
	}
	return true
}

// ListBuckets implements MinioDefinition

func (m Minio) ListBuckets(minioClient *minio.Client) (bucketInfo []minio.BucketInfo, err error) {
	bucketInfo, err = minioClient.ListBuckets(context.Background())
	if err != nil {
		log.Fatalln(err)
	}

	// for _, bucket := range buckets {
	// 	// fmt.Println(bucket.Name)
	// }

	return bucketInfo, err
}

// ListIncompleteUploads implements MinioDefinition
func (m Minio) ListIncompleteUploads(minioClient *minio.Client, bucketName string) map[string]interface{} {
	response := map[string]interface{}{}
	isRecursive := true

	multiPartObjectUpload := minioClient.ListIncompleteUploads(context.Background(), bucketName, "", isRecursive)
	for multiPartObject := range multiPartObjectUpload {
		if multiPartObject.Err != nil {
			// fmt.Println(multiPartObject.Err)
			return response
		}
		response["ListInComplete"] = multiPartObject
		// fmt.Println(multiPartObject)
	}
	return response
}

// ListObjects implements MinioDefinition
func (m Minio) ListObjects(minioClient *minio.Client, buckerName string) map[string]interface{} {
	response := map[string]interface{}{}
	contextCancel, cancel := context.WithCancel(context.Background())

	defer cancel()

	objectSource := minioClient.ListObjects(contextCancel, buckerName, minio.ListObjectsOptions{
		Recursive: true,
		Prefix:    "",
	})

	for object := range objectSource {
		if object.Err != nil {
			// fmt.Println(object.Err)
			return response
		}
		response["listObjects"] = object
		// fmt.Println(object)
	}
	return response
}

// MakeBucket implements MinioDefinition
func (m Minio) MakeBucket(minioClient *minio.Client, bucketName string, location string) {
	err := minioClient.MakeBucket(context.Background(), bucketName, minio.MakeBucketOptions{Region: location})
	if err != nil {
		// Check to see if already exist
		m.BucketExist(minioClient, bucketName)

	} else {
		log.Printf("Successfully created %s\n", bucketName)
	}
}

func (m Minio) BucketExist(minioClient *minio.Client, bucketName string) bool {
	exist, errBucketExist := minioClient.BucketExists(context.Background(), bucketName)
	if errBucketExist == nil && exist {
		// log.Println("already own %\n", bucketName)
		return true
	}

	return false
}

// PutObject implements MinioDefinition
// Uploads objects that are less than 128MiB in a single PUT operation. For objects that are greater than 128MiB
//in size, PutObject seamlessly uploads the object as parts of 128MiB or more depending on the actual file size.
//The max upload size for an object is 5TB.
func (m Minio) PutObject(minioClient *minio.Client, bucketName string, objectName string, filePath string) bool {
	file, err := os.Open(filePath)
	if err != nil {
		// fmt.Println(err)
		return false
	}
	defer file.Close()

	fileStat, err := file.Stat()
	if err != nil {
		// fmt.Println(err)
		return false
	}

	_, err = minioClient.PutObject(context.Background(), bucketName, objectName, file, fileStat.Size(), minio.PutObjectOptions{ContentType: "application/octet-stream"})
	if err != nil {
		// fmt.Println(err)
		return false
	}
	// fmt.Println("Successfully uploaded bytes: ", uploadInfo)
	return true
}

// RemoveBucket implements MinioDefinition
func (m Minio) RemoveBucket(minioClient *minio.Client, bucketName string) bool {
	err := minioClient.RemoveBucket(context.Background(), bucketName)
	if err != nil {
		// fmt.Println(err)
		return false
	}
	return true
}

// RemoveIncompleteUpload implements MinioDefinition
func (m Minio) RemoveIncompleteUpload(minioClient *minio.Client, bucketName string, objectName string) bool {
	err := minioClient.RemoveIncompleteUpload(context.Background(), bucketName, objectName)
	if err != nil {
		// fmt.Println(err)
		return false
	}
	return true
}

// RemoveObject implements MinioDefinition
func (m Minio) RemoveObject(minioClient *minio.Client, bucketName string, objectName string) bool {
	opts := minio.RemoveObjectOptions{
		GovernanceBypass: true,
		// VersionID:        versionId, //Version ID of the object to delete
	}
	err := minioClient.RemoveObject(context.Background(), bucketName, objectName, opts)
	if err != nil {
		// fmt.Println(err)
		return false
	}
	return true
}

// RemoveObjects implements MinioDefinition
func (m Minio) RemoveObjects(minioClient *minio.Client, bucketName string, prefixName string) bool {
	panic("unimplemented")
}

// SelectObjectContent implements MinioDefinition
func (Minio) SelectObjectContent() {
	panic("unimplemented")
}

// SetAppInfo implements MinioDefinition
func (m Minio) SetAppInfo(minioClient *minio.Client, appName string, appVersion string) bool {
	minioClient.SetAppInfo(appName, appVersion)
	return true
}

// SetBucketPolicy implements MinioDefinition
func (m Minio) SetBucketPolicy(minioClient *minio.Client, bucketName string, policy string) bool {
	// policy := `{"Version": "2012-10-17","Statement": [{"Action": ["s3:GetObject"],"Effect": "Allow","Principal": {"AWS": ["*"]},"Resource": ["arn:aws:s3:::my-bucketname/*"],"Sid": ""}]}`

	err := minioClient.SetBucketPolicy(context.Background(), bucketName, policy)
	if err != nil {
		// fmt.Println(err, bucketName)
		return false
	}
	return true
}

// SignUrl implements MinioDefinition
func (m Minio) SignUrl(minioClient *minio.Client, bucketName string, objectName string, filename string) *url.URL {
	reqParams := make(url.Values)
	// reqParams.Set("response-content-disposition", "attachment; filename=\"Dikki Haryadi .jpg\"")

	// Generates a presigned url which expires in a day.
	presignedURL, err := minioClient.PresignedGetObject(context.Background(), bucketName, objectName, time.Duration(1000)*time.Second, reqParams)
	if err != nil {
		// fmt.Println(err)
		return presignedURL
	}

	return presignedURL
}

// StatObject implements MinioDefinition
func (m Minio) StatObject(minioClient *minio.Client, bucketName string, objectName string) (objectInfo minio.ObjectInfo) {
	objInfo, err := minioClient.StatObject(context.Background(), bucketName, objectName, minio.StatObjectOptions{})
	if err != nil {
		// fmt.Println(err)
		return objInfo
	}
	// fmt.Println(objInfo)
	return objInfo
}

// UploadObject implements MinioDefinition
func (m Minio) UploadObject(minioClient *minio.Client, bucketName string, objectName string, filePatch string, contentType string) (bool, error) {
	info, err := minioClient.FPutObject(context.Background(), bucketName, objectName, filePatch, minio.PutObjectOptions{ContentType: contentType})
	if err != nil {
		log.Fatalln(err)
		return false, err
	}

	// log.Printf("Successfully uploaded %s of size %d\n", objectName, info)
	fmt.Println("Successfully uploaded / of size", objectName, info)
	return true, err
}
